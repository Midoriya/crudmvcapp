/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Abdelkrim
 */
public class ArticleDao {
    Connection  conn;
    
    public ArticleDao()
    {
        conn=new Connexion().seConnecter();
    }
    public void insert(Article a)
    {
        try {
            PreparedStatement st=conn.prepareCall("insert into Article values(?,?,?)");
            st.setInt(1,a.getCode());
            st.setString(2,a.getDesignation());
            st.setFloat(3,a.getCode());
            st.executeUpdate();
            
        } catch (SQLException ex) {
           System.err.println(ex.getMessage());
        }
    }
    
    public ResultSet selectall()
    {
        ResultSet res=null;
        try {
            Statement st=conn.createStatement();
            res=st.executeQuery("select * from Article");
        } catch (SQLException ex) {
           System.err.println(ex.getMessage());
        }
        return res;
    }
}
