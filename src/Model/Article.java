/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Abdelkrim
 */
public class Article {
    private String designation;
    private int code;
    private float prix;
   
    private Article()
    {}
public Article(int code,String designation,float prix)
    {
        this.code=code;
        this.prix=prix;
        this.designation=designation;
      }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public float getPrix() {
        return prix;
    }

    public void setPrix(float prix) {
        this.prix = prix;
        
        
    }
    
    
}
