/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author Abdelkrim
 */
public class Connexion {
    
    public Connection seConnecter()
    {Connection Conn=null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Conn= DriverManager.getConnection("jdbc:mysql://localhost:3306/DB","root","");
                    
        } 
        catch (ClassNotFoundException ex) {
            Logger.getLogger(Connexion.class.getName()).log(Level.SEVERE, null, ex);
        } 
        catch (SQLException ex) {
            Logger.getLogger(Connexion.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Conn;
    }
}
