/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crudmvcapp;
import Model.*;
import Controller.*;
import View.*;

/**
 *
 * @author Abdelkrim
 */
public class CrudMvcApp {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        JFCrud jf=new JFCrud();
        ArticleDao A=new ArticleDao();
        CrudCTR ctr=new CrudCTR(jf,A);
        
        jf.setVisible(true);
        jf.setLocationRelativeTo(null);
    }
    
}
