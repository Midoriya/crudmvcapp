/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import Model.*;
import View.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Abdelkrim
 */
public class CrudCTR implements ActionListener {

    private JFCrud vue=new JFCrud();
    private ArticleDao modele=new ArticleDao();
    
    public CrudCTR(JFCrud jf,ArticleDao m)
    {
        vue=jf;
        modele=m;
        vue.getBtnActualiser().addActionListener(this);
        vue.getBtnAjouter().addActionListener(this);
        vue.getBtnModifier().addActionListener(this);
        vue.getBtnSupprimer().addActionListener(this);
        Table();
    }
    
    public void Table()
    {
        DefaultTableModel modelT=new DefaultTableModel();
        vue.getArticleTable().setModel(modelT);
        modelT.addColumn("Code");
        modelT.addColumn("Designation");
        modelT.addColumn("Prix");
        Object[] columns=new Object[3];
       ResultSet res=modele.selectall();
        try {
            while(res.next())
            {
                columns[0]=res.getInt(1);
                columns[1]=res.getString(2);
                columns[2]=res.getFloat(3);
                modelT.addRow(columns);
                        }
        } catch (SQLException ex) {
           System.err.println(ex.getMessage());
        }
    }
    
    
    
    
    
    
    
    
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource()==vue.getBtnAjouter())
        {
            Article A=new Article(Integer.parseInt(vue.getTxtCode().getText()),vue.getTxtDesignation().getText(),Float.parseFloat(vue.getTxtPrix().getText()));
            modele.insert(A);
            JOptionPane.showMessageDialog(null,"Done !");
        }
        
        if(e.getSource()==vue.getBtnActualiser())
        {
            Table();
        }
    }
    
}
